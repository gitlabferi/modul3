import 'package:flutter/material.dart';
import './products.dart';
import './product_control.dart';

class ProductManager extends StatefulWidget{
  final String startingProduct;
  ProductManager({this.startingProduct = 'Sweet Tester'});

  @override
  _ProductManagerState createState(){
    print('[ProductManagerState] createState()');
    return _ProductManagerState();
  }
}

class _ProductManagerState extends State<ProductManager>{
  List<String> _products = [];

@override
void initState() {
  print('[ProductmanagerState] initState()');
  _products.add(widget.startingProduct);
  super.initState();
}

@override
void didUpdateWidget (ProductManager oldWidget) {
  print('[ProductManager State] didUpdateWidget()');
  super.didUpdateWidget(oldWidget);
  
}
@override
void _addProduct(String product) {
  setState(() {
    _products.add('product');
    print(_products);
    });
}

  @override
  Widget build(BuildContext context) {
    print('[ProductManagerState] build()');
    return Column(
      children: <Widget>[
    Container(
            margin: EdgeInsets.all(10.0),
            child: ProductControl (_addProduct),
          ),
          Products(_products),
        ],
      );
  }
}